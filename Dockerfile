FROM alpine

RUN apk add --update curl python bash nodejs nodejs-npm && rm -rf /var/cache/apk/*
RUN npm upgrade npm@5.0.3
RUN npm -v